class: Workflow
cwlVersion: v1.0
id: collect_meta
label: collect_meta
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
inputs:
  - id: surl_list
    type: File
    'sbg:x': -450
    'sbg:y': -142
outputs:
  - id: output
    outputSource:
      - collect_metadata_from_ms/output
    type: 'File[]'
    'sbg:x': 484
    'sbg:y': -61
steps:
  - id: read_surl_list
    in:
      - id: surl_list
        source: surl_list
    out:
      - id: surls
    run: ../steps/read_surl_list.cwl
    label: ReadSurlList
    'sbg:x': -257
    'sbg:y': -153
  - id: collect_metadata_from_ms
    in:
      - id: ms
        source: untar/uncompressed
      - id: format
        default: tar
      - id: access_url
        source: read_surl_list/surls
      - id: project_code
        default: SDC_001
      - id: project_title
        default: Test metadata collection
      - id: optional_args
        default: []
    out:
      - id: output
    run: ../steps/collect_meta_from_ms.cwl
    label: collect_metadata_from_ms
    scatter:
      - ms
      - access_url
    scatterMethod: dotproduct
    'sbg:x': 243
    'sbg:y': -42
  - id: untar
    in:
      - id: tar_file
        source: surl_copy/output
    out:
      - id: uncompressed
    run: ../steps/untar.cwl
    label: untar
    scatter:
      - tar_file
    'sbg:x': 95
    'sbg:y': -286
  - id: surl_copy
    in:
      - id: surl
        source: read_surl_list/surls
    out:
      - id: output
    run: ../steps/surl_copy.cwl
    label: surl_copy
    scatter:
      - surl
    'sbg:x': -83
    'sbg:y': -215
requirements:
  - class: ScatterFeatureRequirement
