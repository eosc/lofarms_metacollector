{
    "$graph": [
        {
            "class": "CommandLineTool",
            "id": "#collect_meta_from_ms.cwl",
            "baseCommand": [
                "lofarmscollectmeta.py"
            ],
            "inputs": [
                {
                    "id": "#collect_meta_from_ms.cwl/collect_metadata_from_ms/ms",
                    "type": "Directory",
                    "inputBinding": {
                        "position": 0
                    }
                },
                {
                    "default": "",
                    "id": "#collect_meta_from_ms.cwl/collect_metadata_from_ms/output_file_name",
                    "type": "string?",
                    "inputBinding": {
                        "position": 1,
                        "valueFrom": "$(inputs.ms.basename + \".json\")"
                    }
                },
                {
                    "id": "#collect_meta_from_ms.cwl/collect_metadata_from_ms/format",
                    "type": "string",
                    "inputBinding": {
                        "position": 3,
                        "valueFrom": "format:$(self)"
                    }
                },
                {
                    "id": "#collect_meta_from_ms.cwl/collect_metadata_from_ms/access_url",
                    "type": "string",
                    "inputBinding": {
                        "position": 3,
                        "valueFrom": "access_url:$(self)"
                    }
                },
                {
                    "id": "#collect_meta_from_ms.cwl/collect_metadata_from_ms/project_code",
                    "type": "string",
                    "inputBinding": {
                        "position": 3,
                        "valueFrom": "proposal_id:$(self)"
                    }
                },
                {
                    "id": "#collect_meta_from_ms.cwl/collect_metadata_from_ms/project_title",
                    "type": "string",
                    "inputBinding": {
                        "position": 3,
                        "valueFrom": "proposal_title:$(self)"
                    }
                },
                {
                    "id": "#collect_meta_from_ms.cwl/collect_metadata_from_ms/optional_args",
                    "type": "string[]?",
                    "inputBinding": {
                        "position": 3
                    }
                }
            ],
            "outputs": [
                {
                    "id": "#collect_meta_from_ms.cwl/collect_metadata_from_ms/output",
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.ms.basename + \".json\")"
                    }
                }
            ],
            "label": "collect_metadata_from_ms",
            "arguments": [
                {
                    "position": 2,
                    "valueFrom": "--opts"
                }
            ],
            "requirements": [
                {
                    "class": "DockerRequirement",
                    "dockerPull": "lofareosc/metadata_collector:latest"
                },
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "$namespaces": {
                "sbg": "https://www.sevenbridges.com/"
            }
        },
        {
            "class": "ExpressionTool",
            "id": "#read_surl_list.cwl",
            "inputs": [
                {
                    "id": "#read_surl_list.cwl/read_surl_file/surl_list",
                    "type": "File",
                    "doc": "input surl file",
                    "inputBinding": {
                        "loadContents": true
                    }
                }
            ],
            "outputs": [
                {
                    "id": "#read_surl_list.cwl/read_surl_file/surls",
                    "type": "string[]"
                }
            ],
            "expression": "${\n  var surl_list = inputs.surl_list.contents.split(\"\\n\");\n  surl_list = surl_list.filter(function (el) { return el != ''; } );\n  return {'surls': surl_list}\n}\n",
            "label": "ReadSurlList",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                }
            ]
        },
        {
            "class": "CommandLineTool",
            "id": "#surl_copy.cwl",
            "baseCommand": [
                "gfal-copy"
            ],
            "inputs": [
                {
                    "id": "#surl_copy.cwl/surl_copy/surl",
                    "type": "string",
                    "inputBinding": {
                        "position": -1
                    }
                }
            ],
            "outputs": [
                {
                    "id": "#surl_copy.cwl/surl_copy/output",
                    "type": "File?",
                    "outputBinding": {
                        "glob": "*"
                    }
                }
            ],
            "doc": "Download a file give a surl",
            "label": "surl_copy",
            "arguments": [
                "."
            ],
            "temporaryFailCodes": [
                127,
                1,
                70
            ]
        },
        {
            "id": "#untar.cwl",
            "label": "untar",
            "class": "CommandLineTool",
            "inputs": [
                {
                    "id": "#untar.cwl/untar/tar_file",
                    "type": "File",
                    "inputBinding": {
                        "position": 0
                    }
                }
            ],
            "outputs": [
                {
                    "id": "#untar.cwl/untar/uncompressed",
                    "type": "Directory",
                    "outputBinding": {
                        "glob": "out/prefactor/results/*"
                    }
                }
            ],
            "baseCommand": [
                "bash",
                "untar.sh"
            ],
            "doc": "Untar a compressed file",
            "requirements": {
                "InitialWorkDirRequirement": {
                    "listing": [
                        {
                            "entryname": "untar.sh",
                            "entry": "#!/bin/bash\nmkdir out\ncd out\ntar -xvf $1\n"
                        }
                    ]
                }
            }
        },
        {
            "class": "Workflow",
            "id": "#main",
            "label": "collect_meta",
            "inputs": [
                {
                    "id": "#main/surl_list",
                    "type": "File",
                    "https://www.sevenbridges.com/x": -450,
                    "https://www.sevenbridges.com/y": -142
                }
            ],
            "outputs": [
                {
                    "id": "#main/output",
                    "outputSource": [
                        "#main/collect_metadata_from_ms/output"
                    ],
                    "type": {
                        "type": "array",
                        "items": "File"
                    },
                    "https://www.sevenbridges.com/x": 484,
                    "https://www.sevenbridges.com/y": -61
                }
            ],
            "steps": [
                {
                    "id": "#main/read_surl_list",
                    "in": [
                        {
                            "id": "#main/read_surl_list/surl_list",
                            "source": "#main/surl_list"
                        }
                    ],
                    "out": [
                        {
                            "id": "#main/read_surl_list/surls"
                        }
                    ],
                    "run": "#read_surl_list.cwl",
                    "label": "ReadSurlList",
                    "https://www.sevenbridges.com/x": -257,
                    "https://www.sevenbridges.com/y": -153
                },
                {
                    "id": "#main/collect_metadata_from_ms",
                    "in": [
                        {
                            "id": "#main/collect_metadata_from_ms/ms",
                            "source": "#main/untar/uncompressed"
                        },
                        {
                            "id": "#main/collect_metadata_from_ms/format",
                            "default": "tar"
                        },
                        {
                            "id": "#main/collect_metadata_from_ms/access_url",
                            "source": "#main/read_surl_list/surls"
                        },
                        {
                            "id": "#main/collect_metadata_from_ms/project_code",
                            "default": "SDC_001"
                        },
                        {
                            "id": "#main/collect_metadata_from_ms/project_title",
                            "default": "Test metadata collection"
                        },
                        {
                            "id": "#main/collect_metadata_from_ms/optional_args",
                            "default": []
                        }
                    ],
                    "out": [
                        {
                            "id": "#main/collect_metadata_from_ms/output"
                        }
                    ],
                    "run": "#collect_meta_from_ms.cwl",
                    "label": "collect_metadata_from_ms",
                    "scatter": [
                        "#main/collect_metadata_from_ms/ms",
                        "#main/collect_metadata_from_ms/access_url"
                    ],
                    "scatterMethod": "dotproduct",
                    "https://www.sevenbridges.com/x": 243,
                    "https://www.sevenbridges.com/y": -42
                },
                {
                    "id": "#main/untar",
                    "in": [
                        {
                            "id": "#main/untar/tar_file",
                            "source": "#main/surl_copy/output"
                        }
                    ],
                    "out": [
                        {
                            "id": "#main/untar/uncompressed"
                        }
                    ],
                    "run": "#untar.cwl",
                    "label": "untar",
                    "scatter": [
                        "#main/untar/tar_file"
                    ],
                    "https://www.sevenbridges.com/x": 95,
                    "https://www.sevenbridges.com/y": -286
                },
                {
                    "id": "#main/surl_copy",
                    "in": [
                        {
                            "id": "#main/surl_copy/surl",
                            "source": "#main/read_surl_list/surls"
                        }
                    ],
                    "out": [
                        {
                            "id": "#main/surl_copy/output"
                        }
                    ],
                    "run": "#surl_copy.cwl",
                    "label": "surl_copy",
                    "scatter": [
                        "#main/surl_copy/surl"
                    ],
                    "https://www.sevenbridges.com/x": -83,
                    "https://www.sevenbridges.com/y": -215
                }
            ],
            "requirements": [
                {
                    "class": "ScatterFeatureRequirement"
                }
            ]
        }
    ],
    "cwlVersion": "v1.0"
}