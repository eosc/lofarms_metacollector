#!/usr/bin/env python3
import logging
from argparse import ArgumentParser
import json
import requests

API_LANDPOINT = 'archive/api/ms_meta/'

def parse_command_line_arguments():
    parser = ArgumentParser(description='collect metadata json file to database')
    parser.add_argument('path', type=str, help='path to metadata file')
    parser.add_argument('--base_url', default='http://localhost:8000')

    parser.add_argument('-v', '--verbose', help='verbose', action='store_true')
    return parser.parse_args()


def setup_logger(is_verbose):
    log_level = logging.INFO
    if is_verbose:
        log_level = logging.DEBUG
    logging.basicConfig(level=log_level,
                        format='%(asctime)-15s %(name)s %(levelname)-8s: %(message)s')
    logger = logging.getLogger('ms_collect_metadata')
    return logger


def ship_single_to_url(url, meta_dict):
    formatted_url = '{}/{}'.format(url, API_LANDPOINT)
    response = requests.post(formatted_url, json=meta_dict)
    if response.status_code // 100 != 2:
        logging.error('failure posting %s - %s ', response.reason, response.content)
    else:
        logging.info('posted successful with id %s', response.json()['id'])

def ship_to_url(base_url, metadata_list):
    for item in metadata_list:
        logging.debug('Shipping item: %s', item)
        ship_single_to_url(base_url, item)
        logging.debug('Shipped item: %s', item)


def load_file(infile_path):
    with open(infile_path, 'r') as f_stream:
        return json.load(f_stream)


def main():
    global logger
    args = parse_command_line_arguments()
    logger = setup_logger(args.verbose)
    logging.info('Reading input file')
    metadata_list = load_file(args.path)
    logging.info('Shipping to url')
    ship_to_url(args.base_url, metadata_list)

if __name__ == '__main__':
    main()
