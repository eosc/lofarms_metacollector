class: CommandLineTool
cwlVersion: v1.0
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
id: surl_copy
baseCommand:
  - gfal-copy
inputs:
  - id: surl
    type: string
    inputBinding:
      position: -1
outputs:
  - id: output
    type: File?
    outputBinding:
      glob: '*'
doc: Download a file give a surl
label: surl_copy
arguments:
  - .
temporaryFailCodes:
  - 127
  - 1
  - 70
