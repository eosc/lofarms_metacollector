id: untar
label: untar
class: CommandLineTool
cwlVersion: v1.0
inputs: 
  - id: tar_file
    type: File
    inputBinding:
      position: 0
    
outputs: 
  - id: uncompressed
    type: Directory
    outputBinding:
      glob: 'out/prefactor/results/*'
baseCommand: 
 - 'bash'
 - 'untar.sh'
doc: 'Untar a compressed file'
requirements:
  InitialWorkDirRequirement:
    listing:
      - entryname: 'untar.sh' 
        entry: |
          #!/bin/bash
          mkdir out
          cd out
          tar -xvf $1

    
