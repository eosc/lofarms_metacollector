class: CommandLineTool
cwlVersion: v1.0
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
id: collect_metadata_from_ms
baseCommand:
  - lofarmscollectmeta.py
inputs:
  - id: ms
    type: Directory
    inputBinding:
      position: 0
  - default: ''
    id: output_file_name
    type: string?
    inputBinding:
      position: 1
      valueFrom: $(inputs.ms.basename + ".json")
  - id: format
    type: string
    inputBinding:
      position: 3
      valueFrom: 'format:$(self)'
  - id: access_url
    type: string
    inputBinding:
      position: 3
      valueFrom: 'access_url:$(self)'
  - id: project_code
    type: string
    inputBinding:
      position: 3
      valueFrom: 'proposal_id:$(self)'
  - id: project_title
    type: string
    inputBinding:
      position: 3
      valueFrom: 'proposal_title:$(self)'
  - id: optional_args
    type: 'string[]?'
    inputBinding:
      position: 3
outputs:
  - id: output
    type: File
    outputBinding:
      glob: $(inputs.ms.basename + ".json")
label: collect_metadata_from_ms
arguments:
  - position: 2
    valueFrom: '--opts'
requirements:
  - class: DockerRequirement
    dockerPull: 'lofareosc/metadata_collector:latest'
  - class: InlineJavascriptRequirement
