#!/usr/bin/env python3
import logging
from argparse import ArgumentParser
from dataclasses import dataclass
from os.path import join as join_path
import json

import numpy
from astropy import units as u
from astropy.coordinates import SkyCoord, EarthLocation, AltAz
from astropy.time import Time
from casacore.tables import table
from casacore.tables.tableutil import tableexists, tableinfo
from dataclasses_json import dataclass_json
from lofarantpos.db import LofarAntennaDatabase
from radio_beam import Beams
from scipy.constants import c as LIGHT_SPEED

logger = None

_MS_TYPE = 'Measurement Set'

UNDEFINED_FOV = -1
UNDEFINED_REGION = ''
UNDEFINED_RESOLUTION = -1
_POL_CODE_TO_STRING = {9: 'XX', 10: 'XY', 11: 'YX', 12: 'YY'}


@dataclass
class TimeMeta:
    t_min: str
    t_max: str
    t_exptime: float
    t_resolution: float
    t_xel: float


@dataclass
class SpectralMeta:
    em_min: float
    em_max: float
    em_xel: float
    em_resolution: float


@dataclass
class SkyFootprintMeta:
    s_ra: float
    s_dec: float
    s_fov: float
    s_region: str
    s_resolution: float
    s_xel1: int
    s_xel2: int


@dataclass_json
@dataclass
class LOFARMetadata(TimeMeta, SpectralMeta, SkyFootprintMeta):
    obs_id: str
    obs_title: str
    subarray: str
    subband_name: str
    obs_release_date: str

    antenna_set: str
    target_name: str
    correlation_products: str
    instrument_name: str

    facility_name: str = 'LOFAR'
    obs_version: str = '0.1'
    obs_specification: str = 'extract_meta'
    calibration_level: int = 1
    dataproduct_type: str = 'visibilities'
    dataproduct_subtype: str = 'calibrated_visibilities'
    o_ucd: str = 'stat.correlation,em.radio'

def main():
    global logger
    args = parse_command_line_arguments()

    logger = setup_logger(args.verbose)
    logging.info('Filtering MS in the specified path')
    mss = filter_ms(args.path)
    logging.info('Reading metadata')

    meta = read_metadata(mss)
    add_anchillary_info(meta, args.opts)

    logging.info('Storing metadata')
    store_metadata(meta, args.out)
    print(meta)


def add_anchillary_info(source, info):
    additional_parameters = {arg.split(':', 1)[0]: arg.split(':', 1)[1] for arg in info}
    for item in source:
        item.update(additional_parameters)

def parse_command_line_arguments():
    parser = ArgumentParser(description='collect metadata from ms')
    parser.add_argument('path', nargs='+', type=str, help='path to ms')
    parser.add_argument('out', type=str, help='output_path')
    parser.add_argument('--opts', type=str, nargs='+', help='additional parameters')
    parser.add_argument('-v', '--verbose', help='verbose', action='store_true')
    return parser.parse_args()


def read_metadata(mss):
    return [obtain_metadata_from_ms(ms) for ms in mss]


def store_metadata(metadata, file_path):
    with open(file_path, 'w') as fstream:
        json.dump(metadata, fstream)


def setup_logger(is_verbose):
    log_level = logging.INFO
    if is_verbose:
        log_level = logging.DEBUG
    logging.basicConfig(level=log_level,
                        format='%(asctime)-15s %(name)s %(levelname)-8s: %(message)s')
    logger = logging.getLogger('ms_collect_metadata')
    return logger


def filter_ms(paths):
    ms_paths = []
    for path in paths:
        if tableexists(path) and tableinfo(path)['type'] == _MS_TYPE:
            ms_paths += [path]
            logging.debug('found path %s', path)
        else:
            logging.debug('skipping file %s', path)

    return ms_paths


def obtain_metadata_from_ms(ms_path):
    obs_id, sap_id, antenna_set, target_name, obs_title, instrument_name, release_date = \
        read_obs_id_sap_id_antenna_set_target_name(ms_path)

    start_time, end_time, exposure, resolution, samples, max_baseline = \
        read_start_time_end_time_exposure_resolution_samples(ms_path)

    subband_name, \
    start_frequency, \
    end_frequency, \
    resolution, \
    n_channels = read_subband_name__start_frequency__end_frequency__resolution__n_channels(ms_path)
    ra, dec = read_ra_dec(ms_path)
    correlation_products = read_correlation_products(ms_path)

    station_names, diameters = read_antenna_field(ms_path)
    beam = stations_common_beam(station_names, diameters, ra, dec, .5 * (end_time + start_time), end_frequency)
    s_region = beam_to_vo_string(beam, ra, dec)
    fov = lofar_station_fov(end_frequency, numpy.max(diameters))

    lofar_meta = LOFARMetadata(
        obs_id=obs_id,
        obs_title=obs_title,
        obs_release_date=release_date,
        subband_name=subband_name,
        subarray=sap_id,
        antenna_set=antenna_set,
        target_name=target_name,
        correlation_products=correlation_products,
        instrument_name=instrument_name,

        t_min=start_time,
        t_max=end_time,
        t_exptime=exposure,
        t_resolution=resolution,
        t_xel=samples,

        em_min=LIGHT_SPEED / start_frequency,
        em_max=LIGHT_SPEED / end_frequency,
        em_xel=n_channels,
        em_resolution=resolution,

        s_ra=ra,
        s_dec=dec,
        s_fov=fov,
        s_region=s_region,
        s_xel1=-1,
        s_xel2=-1,
        s_resolution=lofar_angular_resolution(end_frequency, max_baseline)

    )
    return LOFARMetadata.schema().dump(lofar_meta, many=False)


def read_obs_id_sap_id_antenna_set_target_name(ms_path):
    with open_table(ms_path, 'OBSERVATION') as table:
        obs_id = table.getcell('LOFAR_OBSERVATION_ID', 0)
        sap_id = table.getcell('LOFAR_SUB_ARRAY_POINTING', 0)
        aset = table.getcell('LOFAR_FILTER_SELECTION', 0)
        target_name = table.getcell('LOFAR_TARGET', 0)[0]
        obs_title = table.getcell('LOFAR_PROJECT_TITLE', 0)
        instrument_name = table.getcell('LOFAR_ANTENNA_SET', 0)
        obs_release_data = Time(table.getcell('RELEASE_DATE', 0) / 3600 / 24 , format='mjd', scale='utc').to_value('iso', subfmt='date_hms')
    return obs_id, sap_id, aset, target_name, obs_title, instrument_name, obs_release_data


def read_subband_name__start_frequency__end_frequency__resolution__n_channels(ms_path):
    with open_table(ms_path, 'SPECTRAL_WINDOW') as table:
        subband_name = table.getcell('NAME', 0)
        channels_frequencies = table.getcell('CHAN_FREQ', 0)
        start_frequency, end_frequency = channels_frequencies[0], channels_frequencies[-1]
        resolution = table.getcell('RESOLUTION', 0)[0]
        n_channels = table.getcell('NUM_CHAN', 0)
    return subband_name, start_frequency, end_frequency, resolution, n_channels


def read_ra_dec(ms_path):
    with open_table(ms_path, 'FIELD') as table:
        ra, dec = table.getcell('PHASE_DIR', 0)[0, :]
    return ra, dec


def max_baseline_from_antennas_position(positions):
    x = positions[:, 0]
    y = positions[:, 1]
    z = positions[:, 2]
    dx = x[:, numpy.newaxis] - x[numpy.newaxis, :]
    dy = y[:, numpy.newaxis] - y[numpy.newaxis, :]
    dz = z[:, numpy.newaxis] - z[numpy.newaxis, :]

    distance = numpy.sqrt(dx ** 2 + dy ** 2 + dz ** 2)
    return numpy.max(distance)


def local_frame_for_station(station_name, time):
    db = LofarAntennaDatabase()
    station_location = EarthLocation.from_geocentric(*(db.phase_centres[station_name] * u.m))
    day_in_seconds = 3600 * 24
    time = Time(time / day_in_seconds, format='mjd', scale='utc')
    frame = AltAz(obstime=time, location=station_location)
    return frame


def local_horizon(local_frame, pointing):
    local_coords = pointing.transform_to(local_frame)
    horizon = SkyCoord(local_coords.az, 0 * u.rad, frame=local_frame).transform_to('icrs')
    return horizon


def beam_position_angle(local_frame, pointing):
    horizon = local_horizon(local_frame, pointing)
    return pointing.position_angle(horizon)


def get_beam_axis(station_name, ra, dec, time, fov):
    pointing = SkyCoord(ra=ra * u.deg, dec=dec * u.deg, frame='icrs')
    frame = local_frame_for_station(station_name, time)
    alt = pointing.transform_to(frame).alt
    a_zenith = numpy.sqrt(fov / numpy.pi)
    a_eff = a_zenith / abs(numpy.sin(alt))
    a_eff = a_eff.value
    b_eff = a_zenith
    fov = fov / numpy.sin(alt)
    bpa = beam_position_angle(frame, pointing)
    return a_eff, b_eff, bpa.deg, fov


def stations_common_beam(station_names, diameters, ra, dec, time, frequency):
    a_effs = []
    b_effs = []
    bpas = []
    for station_name, diameter in zip(station_names, diameters):
        fov = lofar_station_fov(frequency, diameter)

        a_eff, b_eff, bpa, fov = get_beam_axis(station_name, ra, dec, time, fov)
        a_effs.append(a_eff)
        b_effs.append(b_eff)
        bpas.append(bpa)
    return Beams(a_effs * u.deg, b_effs * u.deg, bpas * u.deg).smallest_beam()


def read_start_time_end_time_exposure_resolution_samples(ms_path):
    """
    Note that this information is a bit tricky. Might be wise to consider generalize
    such a procedure
    :param ms_path: path to the measurement set
    :return: start_time, end_time, exposure, resolution and number of samples
    """
    with open_table(ms_path, 'ANTENNA') as input_table:
        antenna_positions = input_table.getcol('POSITION')
        n_antennas = len(antenna_positions)
        max_baseline = max_baseline_from_antennas_position(antenna_positions)

    n_baselines = int(.5 * n_antennas * (n_antennas - 1))

    with open_table(ms_path, '') as input_table:
        exposure = input_table.getcell('INTERVAL', 0)
        number_of_data_rows = input_table.nrows()

        first = input_table.getcell('TIME_CENTROID', 0)
        second = input_table.getcell('TIME_CENTROID', n_baselines)

        last = input_table.getcell('TIME_CENTROID', number_of_data_rows - 1)

        start = first - exposure * .5
        end = last + exposure * .5
        resolution = second - first
        n_time_samples = number_of_data_rows / n_baselines

    return start, end, exposure, resolution, n_time_samples, max_baseline


def read_correlation_products(ms_path):
    with open_table(ms_path, 'POLARIZATION') as input_table:
        polarizations = input_table.getcell('CORR_TYPE', 0)
        return ','.join(map(lambda x: _POL_CODE_TO_STRING[x], polarizations))


def read_antenna_field(ms_path):
    with open_table(ms_path, 'ANTENNA') as input_table:
        dish_diameters = input_table.getcol('DISH_DIAMETER')
        station_names = input_table.getcol('NAME')
        return station_names, dish_diameters


def open_table(ms_path, table_name):
    table_path = join_path(ms_path, table_name)
    return table(table_path)


def lofar_angular_resolution(freq, baseline_length, alpha=1.02):
    wavelenght = LIGHT_SPEED / freq
    fwhm = alpha * wavelenght / baseline_length / numpy.pi * 180
    return fwhm


def lofar_station_fov(freq, diameter):
    alpha = 1.02
    fwhm = lofar_angular_resolution(freq, diameter, alpha=alpha)
    fov = numpy.pi * fwhm ** 2 * .25
    return fov


def beam_to_vo_string(beam, ra, dec):
    stc_s = 'Ellipse ICRS {ra} {dec} {a_eff} {b_eff} {bpa}'.format(ra=ra, dec=dec, a_eff=beam.major.to(u.deg).value,
                                                                   b_eff=beam.minor.to(u.deg).value,
                                                                   bpa=beam.pa.to(u.deg).value)
    return stc_s


if __name__ == '__main__':
    main()
