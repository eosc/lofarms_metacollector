from lofarmscollectmeta import *
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
import astropy.coordinates as coord
import sys

ms = sys.argv[1]
ra, dec = read_ra_dec(ms)
pointing = coord.SkyCoord(ra=115*u.deg, dec=50*u.deg)
time = 5030946001
frame = local_frame_for_station('CS001HBA0', time)
local_coords = pointing.transform_to(frame)
local_hor = local_horizon(frame, pointing)

a, b, bpa = get_beam_axis('CS001HBA0', pointing.ra.degree, pointing.dec.degree, time, 1)

print(bpa.degree)
beam = Ellipse(xy=(pointing.ra/u.deg, pointing.dec/u.deg),
                width=a, height=b,
                angle=bpa.degree-90)
print(bpa.deg)
to_rad = 180 / numpy.pi

fig, ax = plt.subplots(subplot_kw={'aspect': 'equal'})
print(local_hor)
ax.scatter(local_hor.ra, local_hor.dec)

ax.scatter(pointing.ra, pointing.dec)
delta_ra = pointing.ra - 1 * numpy.cos(-bpa-90*u.deg) * u.deg
delta_dec = pointing.dec - 1 * numpy.sin(-bpa-90*u.deg) * u.deg

ax.scatter(delta_ra, delta_dec, label='bpa')

ax.scatter(local_hor.ra, local_hor.dec, label='horizon')
ax.plot([pointing.ra/u.deg, local_hor.ra/u.deg], [pointing.dec/u.deg, local_hor.dec/u.deg], '-', label='line')
plt.legend()

ax.add_artist(beam)
alts = numpy.linspace(-90, 90, 1000)

coords = SkyCoord(az=local_coords.az, alt=alts*u.deg, frame=frame).transform_to('icrs')

plt.xlabel('ra')
plt.ylabel('dec')

plt.plot(coords.ra, coords.dec, '-')
plt.show()